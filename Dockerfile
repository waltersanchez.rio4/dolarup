FROM python:3.8

ENV PYTHONUNBUFFERED 1

COPY . /dolarup

WORKDIR /dolarup

COPY requirements.txt ./
RUN pip install flask==1.1.2 & set FLASK_APP=main.py & set FLASK_ENV=development
RUN pip install --no-cache-dir -r requirements.txt

EXPOSE 1785

CMD [ "python", "main.py" ] 