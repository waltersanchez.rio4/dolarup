from flask import Flask, json
from rio4_dolar import creation_table_dolar_rio4, update_table_rio4
from ambito_dolar import creation_table_dolar_ambito, update_table_ambito
from apscheduler.schedulers.background import BackgroundScheduler

app = Flask(__name__)
app.config["SQLALCHEMY_DATABASE_URI"] = "sqlite:///sqlite_example.db"
# db = SQLAlchemy(app)
# migrate = Migrate(app, db)
# db.init_app(app)
# db.create_all()


@app.route('/', methods=['GET', 'POST'])
def service_info():
    return "Service Dolarup Run!"

@app.route('/creation_DB', methods=['GET'])
def creation_DB():
    response = creation_DB_ambito()
    _response = creation_DB_rio4()
    if response.status==200 and _response.status==200:
        response = app.response_class(
            response=json.dumps({"status" : "Success"}),
            status=200,
            mimetype='application/json'
        )
        return response
    else:
        response = app.response_class(
            response=json.dumps({"status" : "Fail"}),
            status=400,
            mimetype='application/json'
        )
        return response

@app.route('/creation_DB_ambito', methods=['GET', 'POST'])
def creation_DB_ambito():
    with app.app_context():
        response = creation_table_dolar_ambito()
        
        response = app.response_class(
            response=json.dumps({"status": response}),
            status=200,
            mimetype='application/json'
        )
        return response

@app.route('/creation_DB_rio4', methods=['GET'])
def creation_DB_rio4():
    with app.app_context():
        response = creation_table_dolar_rio4()
        response = app.response_class(
            response=json.dumps({"status" : response}),
            status=200,
            mimetype='application/json'
        )
        return response



@app.route('/check_price_ambito', methods=['GET', 'POST'])
def dolar_check_price():
    response = update_table_ambito()
    response = app.response_class(
        response=json.dumps({"status" : response}),
        status=200,
        mimetype='application/json'
    )
    return response

@app.route('/check_price_rio4', methods=['GET', 'POST'])
def dolar_check_price_rio4():
    response = update_table_rio4()
    response = app.response_class(
        response=json.dumps({"status" : response}),
        status=200,
        mimetype='application/json'
    )
    return response

def check_price_dolar_variacion():
    response = dolar_check_price()
    _response = dolar_check_price_rio4()
    print("Request dolarup:", "ok")

scheduler = BackgroundScheduler()
scheduler.add_job(func=check_price_dolar_variacion, trigger="interval", seconds=300)
scheduler.start()

if __name__ == '__main__':
   app.run(host='0.0.0.0',port=1785, debug=True)