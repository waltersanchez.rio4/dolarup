import sqlite3
from utils import function_to_parser_str, scrap_html_dolar_rio4, send_whatsapp_message
from datetime import datetime

URL_ROOT = "https://www.infodolar.com/cotizacion-dolar-localidad-rio-cuarto-provincia-cordoba.aspx" #Fuente 1

def creation_table_dolar_rio4():

    two, three, four, _two, _three, _four = scrap_html_dolar_rio4(URL_ROOT)
        
    ##################################################################################
    conn = sqlite3.connect('dolarup.db')
    c = conn.cursor()    
    ##################################################################################
    one = "nacion_rio4"
    five = True
    six = datetime.now()
    # Insert a row of data
    c.execute("INSERT INTO DolarInfo (type_dolar, compra, venta, variacion, class_variacion, date_time) values (?, ?, ?, ?, ?, ?)", (one, two, three, four, five, six))
    
    ##################################################################################
    _one = "blue_rio4"
    _five = True
    _six = datetime.now()
    # Insert a row of data
    c.execute("INSERT INTO DolarInfo (type_dolar, compra, venta, variacion, class_variacion, date_time) values (?, ?, ?, ?, ?, ?)", (_one, _two, _three, _four, _five, _six))
    conn.commit()
    conn.close()
    ##################################################################################

    return 200


def update_table_rio4():

    two, three, four, _two, _three, _four, _status_code = scrap_html_dolar_rio4(URL_ROOT)
    
    if _status_code!=200:
        return _status_code

    conn = sqlite3.connect('dolarup.db')
    c = conn.cursor()

    for row in c.execute('SELECT * FROM DolarInfo'):

        if row[0]=="nacion_rio4":

            referencia_compra = function_to_parser_str(row[1])
            referencia_venta = function_to_parser_str(row[2])
            actual_compra = function_to_parser_str(two)
            actual_venta = function_to_parser_str(three)
            if referencia_compra==actual_compra and referencia_venta==actual_venta:
                response_wap = "No_changes"
            else:
                class_variacion = True if referencia_venta < actual_venta else False
                update = (two, three, four, class_variacion, datetime.now(), row[0])
                #Updating the records
                sql = ''' UPDATE DolarInfo
                    SET compra = ? ,
                        venta = ? ,
                        variacion = ? , 
                        class_variacion = ? , 
                        date_time = ?
                    WHERE type_dolar = ?'''
                c.execute(sql, update)
                conn.commit()
                send_whatsapp_message(row[0], two, three, four, str(class_variacion))
                    
                

        if row[0]=="blue_rio4":

            referencia_compra = function_to_parser_str(row[1])
            referencia_venta = function_to_parser_str(row[2])
            actual_compra = function_to_parser_str(_two)
            actual_venta = function_to_parser_str(_three)
            if referencia_compra==actual_compra and referencia_venta==actual_venta:
                response_wap = "No_changes_in_rio4"
                print(response_wap)
            else:
                _class_variacion = True if referencia_venta < actual_venta else False
                update = (_two, _three, _four, _class_variacion, datetime.now(), row[0])
                #Updating the records
                sql = ''' UPDATE DolarInfo
                    SET compra = ? ,
                        venta = ? ,
                        variacion = ? , 
                        class_variacion = ? , 
                        date_time = ?
                    WHERE type_dolar = ?'''
                c.execute(sql, update)
                conn.commit()
                send_whatsapp_message(row[0], _two, _three, _four, str(_class_variacion))


    conn.close()

    return 200