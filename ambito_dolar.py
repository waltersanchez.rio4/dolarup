import requests
import sqlite3
from utils import function_to_parser_str, send_whatsapp_message

URL_ROOT_TWO = "https://mercados.ambito.com//dolar" #Fuente 1
URL_AMBITO_OFICIAL = URL_ROOT_TWO +'/nacion/variacion'                  #Cotizacion dólar oficial
URL_AMBITO_BLUE = URL_ROOT_TWO + "/informal/variacion"               #Cotizacion dólar blue

def creation_table_dolar_ambito():
        
        ##################################################################################
        conn = sqlite3.connect('dolarup.db')
        c = conn.cursor()
        c.execute('''CREATE TABLE DolarInfo
            (type_dolar, compra, venta, variacion, class_variacion, date_time)''')
        
        ##################################################################################
        url= URL_AMBITO_OFICIAL
        response = requests.get(url=url)
        one = "nacion_ambito"
        two = response.json()["compra"]
        three = response.json()["venta"]
        four = response.json()["variacion"]
        five = True
        six = response.json()["fecha"]
        # Insert a row of data
        c.execute("INSERT INTO DolarInfo (type_dolar, compra, venta, variacion, class_variacion, date_time) values (?, ?, ?, ?, ?, ?)", (one, two, three, four, five, six))
        
        ##################################################################################
        _url = URL_AMBITO_BLUE
        _response = requests.get(url=_url)
        _one = "blue_ambito"
        _two = _response.json()["compra"]
        _three = _response.json()["venta"]
        _four = _response.json()["variacion"]
        _five = True
        _six = _response.json()["fecha"]
        # Insert a row of data
        c.execute("INSERT INTO DolarInfo (type_dolar, compra, venta, variacion, class_variacion, date_time) values (?, ?, ?, ?, ?, ?)", (_one, _two, _three, _four, _five, _six))
        conn.commit()
        conn.close()
        ##################################################################################

        # url_two = URL_ROOT_TWO + "/informal/variacion"               #Cotizacion dólar blue
        # response_two = requests.get(url=url_two)
        # from models import DolarInfo
        # nuevo_two = DolarInfo(
        #                 type_dolar = "blue_ambito",
        #                 compra = response_two.json()["compra"],
        #                 venta = response_two.json()["venta"],
        #                 variacion = response_two.json()["variacion"],
        #                 class_variacion = response_two.json()["class-variacion"],
        #                 date_time = response_two.json()["fecha"]
        #                 )
        # db.session.add(nuevo_two)
        # db.session.commit() 
        ##################################################################################

        return 200


def update_table_ambito():

    URL_ROOT_TWO = "https://mercados.ambito.com//dolar" #Fuente 1
    _status_code = 200

    conn = sqlite3.connect('dolarup.db')
    c = conn.cursor()

    for row in c.execute('SELECT * FROM DolarInfo'):
        if row[0]=="nacion_ambito" or row[0]=="blue_ambito":
            if row[0]=="nacion_ambito":
                url= URL_AMBITO_OFICIAL
            else:
                url = URL_AMBITO_BLUE

            response = requests.get(url=url)
            if response.status_code!=200:
                print("Request fail: ", url, "table: ", row[0])
                _status_code = 400
                continue

            referencia_compra = function_to_parser_str(row[1])
            referencia_venta = function_to_parser_str(row[2])
            actual_compra = function_to_parser_str(response.json()["compra"])
            actual_venta = function_to_parser_str(response.json()["venta"])
            if referencia_compra==actual_compra and referencia_venta==actual_venta:
                response_wap = "No_changes_in_ambito"
                print(response_wap)
            else:
                class_variacion = True if response.json()["class-variacion"]=="up" else False
                update = (response.json()["compra"], response.json()["venta"], response.json()["variacion"], class_variacion, response.json()["fecha"], row[0])
                #Updating the records
                sql = ''' UPDATE DolarInfo
                    SET compra = ? ,
                        venta = ? ,
                        variacion = ? , 
                        class_variacion = ? , 
                        date_time = ?
                    WHERE type_dolar = ?'''
                c.execute(sql, update)
                conn.commit()

                send_whatsapp_message(row[0], response.json()["compra"], response.json()["venta"], response.json()["variacion"], response.json()["class-variacion"])

    conn.close()

    return _status_code