from flask_sqlalchemy import SQLAlchemy
import enum
from main import db
#db = SQLAlchemy()

class OptionsDolar(enum.Enum):
    NACION_AMBITO = 'nacion_ambito'
    BLUE_AMBITO = 'blue_ambito'
    OFICIAL_RIO4 = 'oficial_rio4'
    BLUE_RIO4 = 'blue_rio4'


class DolarInfo(db.Model):
    
    __tablename__ = 'dolar_info'

    id = db.Column(db.Integer, primary_key=True)
    type_dolar = db.Column(db.Enum(OptionsDolar), nullable=True)
    compra = db.Column(db.String(200), nullable=True)
    venta = db.Column(db.String(200), nullable=True)
    variacion = db.Column(db.String(200), nullable=False)
    class_variacion = db.Column(db.Boolean, default=False)
    date_time = db.Column(db.String(200), nullable=True)