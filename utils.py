import requests
import pywhatkit
from bs4 import BeautifulSoup
from config import config

def function_to_parser_str(str_obj):
    result = str_obj.split(',')
    number_dec_str = "0." + result[1]
    number_int = int(result[0])
    number_dec = float(number_dec_str)
    result_sum = number_int + number_dec
    return result_sum


def scrap_html_dolar_rio4(URL_ROOT):

    response = requests.get(url=URL_ROOT)
    if response.status_code!=200:
        return None, None, None, None, None, None, response.status_code
    soup = BeautifulSoup(response.content, "html.parser")
    job_elements = soup.find_all(class_="colCompraVenta")
    count = count_one = count_two = count_three = 0
    for job_element in job_elements[0]:
        if count==0:
            string = job_element.strip()
            two = string.replace("$","")
            #two = function_to_parser_str(string)
        count+=1

    for job_element in job_elements[1]:
        if count_one==0:
            string = job_element.strip()
            three = string.replace("$","")
            #three = function_to_parser_str(string)
        if count_one==1:
            string = job_element.text
            four = string.replace("$","")
            #four = function_to_parser_str(string)
        count_one+=1

    for job_element in job_elements[4]:
        if count_two==0:
            string = job_element.strip()
            _two = string.replace("$","")
            #_two = function_to_parser_str(string)
        count_two+=1
    for job_element in job_elements[5]:
        if count_three==0:
            string = job_element.strip()
            _three = string.replace("$","")
            #_three = function_to_parser_str(string)
        if count_three==1:
            string = job_element.text
            _four = string.replace("-$","")
            #_four = function_to_parser_str(string)
        count_three+=1

    return two, three, four, _two, _three, _four, response.status_code


def send_whatsapp_message(dolar_type, two, three, four, class_variacion):
    

    text_to_send = dolar_type.title().center(30, "-") 
    text_to_send_info = "compra: " + two + ",  venta: " + three
    complemented_info = "variacion: " + four + ", class_variacion: " + class_variacion
    final_text = f'''{text_to_send}\n\n{text_to_send_info}\n{complemented_info}'''
    response_wap = pywhatkit.sendwhatmsg_instantly("+5493584828001", str(final_text))
    #response_wap = pywhatkit.sendwhatmsg_instantly("+5493584345335", str(final_text))
    return 200