import os
import time
import pytz
from decouple import config
from dotenv import load_dotenv
load_dotenv()

class Config:
    ENV = os.getenv("FLASK_ENV")
    SECRET_KEY = os.getenv("SECRET_KEY")

    DEBUG = os.getenv("DEBUG")

    NUMBER_WHATSAPP = os.getenv("NUMBER_WHATSAPP")


config = Config
