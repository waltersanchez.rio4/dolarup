# Sobre la app
    Esta aplicacion esta pensada para consultar cada 5 minutos el valor del dolar oficial y blue
    en ambito financiero y en la pagina mas referenciable de Rio Cuarto - Cba.
    En caso de haber cambios en los valores, envia la informacion a un celular predefinido (whatsapp)


# Crear enviroment
    Primero es necesario instalar virtual env y activarlo:
    - apt install python3.8-venv
    - python3 -m venv dolarup-env
    - sudo chmod -R 777 dolarup-env
    -dolarup-env/bin/activate


# Para instalar en el env las librerias necesarias:
    pip install -r requirements.txt
    pip install flask & set FLASK_APP=main.py & set FLASK_ENV=development

    En caso de error instale manualmente:
        flask //pip install flask & set FLASK_APP=main.py & set FLASK_ENV=development
        requests==2.27.1 // pip install requests==2.27.1
        pywhatkit==2.1.1 //pip install pywhatkit
        beautifulsoup4 // pip install beautifulsoup4
        Flask-SQLAlchemy //pip install Flask-SQLAlchemy
        APScheduler==3.6.3 //pip install APScheduler
        Flask-Migrate //pip install Flask-Migrate

# Para crear las tablas de referencia inicial:
En el navegador: localhost:1785/creation_DB

# Iniciar la applicacion
    python3 main.py


# Aclaraciones
    Docker aun no esta incorporado, por la limitacion de la libreria pywhatkit


# Fuentes de consulta:
#Fuente 1: https://github.com/Castrogiovanni20/api-dolar-argentina
#Fuente 2: Ambito Financiero
#URL_ROOT_ONE = "https://api-dolar-argentina.herokuapp.com" #Fuente 1
#URL_ROOT_TWO = "https://mercados.ambito.com//dolar" #Fuente 2

# librerias para que se podrian incorporar
//whatsapp-web.js //npm i whatsapp-web.js
//qrcode-terminal //      npm i qrcode-terminal